# ZAcaWordle (Fork Benoit Averty)

Project Wordle to learn JAVA with Zenika Academy

## Example of a game
![example-game.png](./example-game.png)

## Class diagram
![uml-wordle.png](./uml-wordle.png)

## TODO

Exercice 1 - Springify :
1. pom.xml
2. Créer les Beans
3. Modifier le Main en CommandLineRunner

Exercice 2 - Api HTTP
1. pom.xml
2. Controller -> définir les endpoints (1 route => 1 méthode)
3. Implémentation API

Exercice 3 - Gestion des exceptions
- améliorer la gestion des erreurs (mauvaises requêtes, partie non trouvée...) en suivant ce tuto :
https://medium.com/@jovannypcg/understanding-springs-controlleradvice-cd96a364033f
- Et/ou chercher plus d'infos sur les annotations @ControllerAdvice et @ExceptionHandler

Exercice 4 - Utilisation API Dictionnaire
- Récupérer les mots depuis l'Api scrabble directement plutôt que dans les fichiers txt (DictionaryService)
- Créer 2 Profiles (1 avec la méthode fichier txt ou 1 avec l'api)
- Voir ``RestTemplate``
