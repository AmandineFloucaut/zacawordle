package com.zenika.academy.barbajavas;

/*@Component
@Profile("!test")
public class WordleAppRun implements CommandLineRunner {

    ConsoleColorDisplayer consoleColorDisplayer;
    DictionaryService dictionaryService;
    GameRepository gameRepository;
    GameManager gameManager;
    I18n i18n;

    public WordleAppRun(ConsoleColorDisplayer consoleColorDisplayer, DictionaryService dictionaryService, GameRepository gameRepository,
                        GameManager gameManager, I18n i18n){
        this.consoleColorDisplayer = consoleColorDisplayer;
        this.dictionaryService = dictionaryService;
        this.gameRepository = gameRepository;
        this.gameManager = gameManager;
        this.i18n = i18n;
    }

    @Override public void run(String... args) throws Exception {

        Scanner scanner = new Scanner(System.in);

        // Game
        boolean stop = false;
        while (!stop) {
            System.out.println("Démarrer une partie : quelle longueur de mot ?");
            int wordLength = Integer.parseInt(scanner.nextLine());
            System.out.println("Combien de tentatives maximum ?");
            int nbAttempts = Integer.parseInt(scanner.nextLine());

            //Game game = this.gameManager.startNewGame(wordLength, nbAttempts);
            System.out.println(this.i18n.getMessage("try_to_guess", wordLength));
            while (game.getGameState().equals(IN_PROGRESS)) {
                System.out.println(this.consoleColorDisplayer.format(game.getRounds(), true));
                System.out.println(i18n.getMessage("nb_try_left", game.getAttemptsLeft()));

                String guess = scanner.nextLine();
                try {
                    game = this.gameManager.attempt(game.getTid(), guess);
                } catch (IllegalWordException e) {
                    System.out.println(i18n.getMessage("word_not_in_dictionary"));
                }
                catch (BadLengthException e) {
                    System.out.println(i18n.getMessage("nb_letters_word_try", game.getWordLength()));
                }
            }
            System.out.println(game.getGameState().equals(WIN) ? i18n.getMessage("victory", game.getRounds().size()) : i18n.getMessage("fail"));
            System.out.println(consoleColorDisplayer.format(game.getRounds(), false));
            System.out.println(i18n.getMessage("word_to_guess_was", game.getWord()));

            System.out.println(i18n.getMessage("would_you_replay"));
            stop = scanner.nextLine().trim().equalsIgnoreCase(i18n.getMessage("no"));
        }
    }
}*/
