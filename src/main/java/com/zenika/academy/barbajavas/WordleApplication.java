package com.zenika.academy.barbajavas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Scanner;

import static com.zenika.academy.barbajavas.wordle.domain.model.GameState.IN_PROGRESS;
import static com.zenika.academy.barbajavas.wordle.domain.model.GameState.WIN;

@SpringBootApplication
public class WordleApplication {
    public static void main(String[] args) throws Exception {

        SpringApplication.run(WordleApplication.class, args);
    }
}
