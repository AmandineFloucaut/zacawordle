package com.zenika.academy.barbajavas.wordle.configuration;

import com.zenika.academy.barbajavas.wordle.domain.service.i18n.I18n;
import com.zenika.academy.barbajavas.wordle.domain.service.i18n.I18nFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanPrinterConfig {

    @Bean
    I18n generateI18n(@Value("${i18n.language}") String lang) throws Exception {
        return I18nFactory.getI18n(lang);
    }
}
