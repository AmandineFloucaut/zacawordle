package com.zenika.academy.barbajavas.wordle.domain.service.exceptions;

import com.zenika.academy.barbajavas.wordle.domain.service.BadLengthException;
import com.zenika.academy.barbajavas.wordle.domain.service.IllegalWordException;
import com.zenika.academy.barbajavas.wordle.domain.service.i18n.I18n;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GameExceptionsController {

    @Autowired
    private I18n i18n;

    // DOC -- https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/http/ResponseEntity.html
    @ExceptionHandler(IllegalWordException.class)
    public ResponseEntity<ApiErrorDto> exceptionWordNotExist(IllegalWordException exception){
        ApiErrorDto currentApiErrorDto = new ApiErrorDto(HttpStatus.BAD_REQUEST, this.i18n.getMessage("word_not_in_dictionary"));
        return new ResponseEntity<>(currentApiErrorDto, currentApiErrorDto.getHttpCode());
    }

    @ExceptionHandler(BadLengthException.class)
    public ResponseEntity<ApiErrorDto> exceptionWordWithErrorLength(BadLengthException exception){
        ApiErrorDto currentApiErrorDto = new ApiErrorDto(HttpStatus.BAD_REQUEST, this.i18n.getMessage("nb_letters_word_try"));
        return new ResponseEntity<>(currentApiErrorDto, currentApiErrorDto.getHttpCode());
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<ApiErrorDto> exceptionForGameNotExist(IllegalArgumentException exception){
        ApiErrorDto currentApiErrorDto = new ApiErrorDto(HttpStatus.BAD_REQUEST, this.i18n.getMessage("game_not_exist"));
        return new ResponseEntity<>(currentApiErrorDto, currentApiErrorDto.getHttpCode());
    }
}
