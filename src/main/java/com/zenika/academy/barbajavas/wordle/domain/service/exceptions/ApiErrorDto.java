package com.zenika.academy.barbajavas.wordle.domain.service.exceptions;

import com.zenika.academy.barbajavas.wordle.domain.service.i18n.I18n;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;

public class ApiErrorDto {

    private final HttpStatus httpCode;
    private final String message;

    public ApiErrorDto(HttpStatus httpCode, String message){
        this.httpCode = httpCode;
        this.message = message;
    }

    public HttpStatus getHttpCode() {
        return httpCode;
    }

    public String getMessage() {
        return message;
    }

}
