package com.zenika.academy.barbajavas.wordle.webcontroller;

import com.zenika.academy.barbajavas.wordle.application.GameManager;
import com.zenika.academy.barbajavas.wordle.domain.model.Game;
import com.zenika.academy.barbajavas.wordle.domain.service.BadLengthException;
import com.zenika.academy.barbajavas.wordle.domain.service.GuessDto;
import com.zenika.academy.barbajavas.wordle.domain.service.IllegalWordException;
import com.zenika.academy.barbajavas.wordle.domain.service.i18n.I18n;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class GameController {

    private final GameManager gameManager;

    @Autowired
    public GameController(GameManager gameManager, I18n i18n){
        this.gameManager = gameManager;
    }

    @PostMapping("/games")
    public Game createGame(@RequestParam(value = "wordLength") int wordLength, @RequestParam(value = "maxAttempts") int maxAttempts){
        return this.gameManager.startNewGame(wordLength, maxAttempts);
    }

    // DOC -- https://www.baeldung.com/spring-pathvariable / https://codingnconcepts.com/spring-boot/jackson-json-request-response-mapping/
    @PostMapping("/games/{gameTid}")
    public Game guessTheWord(@PathVariable("gameTid") String gameTid, @RequestBody GuessDto wordToGuess) throws BadLengthException,
            IllegalWordException {
        return this.gameManager.attempt(gameTid, wordToGuess.getWordToGuess());
    }

    @GetMapping("/games/{gameTid}")
    public Game getGameById(@PathVariable("gameTid") String gameTid) throws BadLengthException,
            IllegalWordException {
        return this.gameManager.findGameById(gameTid);
    }

    @GetMapping("/games/{gameTid}/state")
    public String getStateCurrGame(@PathVariable("gameTid") String gameTid) throws BadLengthException,
            IllegalWordException {
        return this.gameManager.getStateOfCurrGame(gameTid);
    }
}

