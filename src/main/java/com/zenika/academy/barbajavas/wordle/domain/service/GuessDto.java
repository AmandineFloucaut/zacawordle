package com.zenika.academy.barbajavas.wordle.domain.service;

import com.fasterxml.jackson.annotation.JsonCreator;

public class GuessDto {

    private final String wordToGuess;

    @JsonCreator
    public GuessDto(String wordToGuess){
        this.wordToGuess = wordToGuess;
    }

    public String getWordToGuess() {
        return wordToGuess;
    }

}
