package com.zenika.academy.barbajavas.wordle.game;

import com.zenika.academy.barbajavas.wordle.domain.model.Game;
import com.zenika.academy.barbajavas.wordle.game.mock.DisplayerMock;
import com.zenika.academy.barbajavas.wordle.game.mock.I18nMock;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.test.context.ActiveProfiles;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@ActiveProfiles("test")
public class GameTest {

    @Autowired
    I18nMock mockI18n;

    @Autowired
    DisplayerMock mockDisplayer;


    @Test
    void testGameSuccessFirstRound() {
       /* List<String> wordsToTry = new ArrayList<>();
        wordsToTry.add("TEST");
        I18nMock mockI18n = new I18nMock("TEST", List.of(new String[]{"TEST"}));
*/
        /*Game game = new Game(this.mockI18n, wordsToTry.iterator(), this.mockDisplayer);
        List<RoundResult> result = game.play();

        assertEquals(1, result.size());
        assertTrue( result.get(0).isWin());

        assertTrue(Arrays.stream(result.get(0).getValidationLetters()).allMatch(v -> v==ValidationLetter.GOOD_POSITION));

        char[] lettersTry = new char[] {'T','E','S','T'};
        assertArrayEquals(lettersTry, result.get(0).getLetters());*/
    }

    @Test
    void testGameSuccessFirstRoundButFirstTryNotInDictionary() {
//        List<String> wordsToTry = new ArrayList<>();
//        wordsToTry.add("PLOP");
//        wordsToTry.add("TEST");
//        I18nMock mockI18n = new I18nMock("TEST", List.of(new String[]{"TEST"}));
//
//        Game game = new Game(mockI18n, wordsToTry.iterator(), new DisplayerMock());
//        List<RoundResult> result = game.play();
//
//        assertEquals(1, result.size());
//        assertTrue( result.get(0).isWin());
//
//        assertTrue(Arrays.stream(result.get(0).getValidationLetters()).allMatch(v -> v==ValidationLetter.GOOD_POSITION));
//
//        char[] lettersTry = new char[] {'T','E','S','T'};
//        assertArrayEquals(lettersTry, result.get(0).getLetters());
    }

    @Test
    void testGameSuccessFirstRoundButFirstWordNotSameLength() {
//        List<String> wordsToTry = new ArrayList<>();
//        wordsToTry.add("PAF");
//        wordsToTry.add("TEST");
//        I18nMock mockI18n = new I18nMock("TEST", List.of(new String[]{"TEST", "PAF"}));
//
//        Game game = new Game(mockI18n, wordsToTry.iterator(), new DisplayerMock());
//        List<RoundResult> result = game.play();
//
//        assertEquals(1, result.size());
//        assertTrue( result.get(0).isWin());
//
//        assertTrue(Arrays.stream(result.get(0).getValidationLetters()).allMatch(v -> v==ValidationLetter.GOOD_POSITION));
//
//        char[] lettersTry = new char[] {'T','E','S','T'};
//        assertArrayEquals(lettersTry, result.get(0).getLetters());
    }

    @Test
    void testGameSuccessFail6Rounds() {
//        List<String> wordsToTry = new ArrayList<>();
//        wordsToTry.add("PLOP");
//        wordsToTry.add("PLOP");
//        wordsToTry.add("PLOP");
//        wordsToTry.add("PLOP");
//        wordsToTry.add("PLOP");
//        wordsToTry.add("PLOP");
//        I18nMock mockI18n = new I18nMock("TEST", List.of(new String[]{"TEST", "PLOP"}));
//
//        Game game = new Game(mockI18n, wordsToTry.iterator(), new DisplayerMock());
//        List<RoundResult> result = game.play();
//
//        assertEquals(6, result.size());
//        assertTrue( result.stream().noneMatch(RoundResult::isWin));
    }

    @TestConfiguration
    static class testConfiguration {
      /*  @Bean
        @Primary
        public*/
    }
}
